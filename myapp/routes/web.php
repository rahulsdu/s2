<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login',[HomeController::class,'index']);
Route::post('/login',[HomeController::class,'login']);

Route::get('/logout', [HomeController::class, 'logout'])->name('logout');
Route::get('/home', [HomeController::class, 'user'])->name('home');



Route::group(['prefix' => 'admin',  'middleware' => 'is_admin'], function()
{
    Route::get('home', [HomeController::class, 'adminHome'])->name('admin.home');
    Route::get('users', [AdminController::class, 'users'])->name('admin.users');
    Route::get('category', [AdminController::class, 'category'])->name('admin.category');
    Route::get('add-category', [AdminController::class, 'addCategory'])->name('admin.addcategory');
    Route::post('create-category', [AdminController::class, 'addCategory'])->name('admin.createcategory');
    Route::get('subcategory', [AdminController::class, 'subcategory'])->name('admin.subcategory');
    Route::get('add-subcategory', [AdminController::class, 'addSubCategory'])->name('admin.addsubcategory');
    Route::post('create-subcategory', [AdminController::class, 'addSubCategory'])->name('admin.createsubcategory');
    Route::get('cat-delete/{id}', [AdminController::class, 'catDelete'])->name('admin.catdelete');
    Route::get('subcat-delete/{id}', [AdminController::class, 'subcatDelete'])->name('admin.subcatdelete');
});
