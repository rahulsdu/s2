@extends('layouts.app')
   
@section('content')
<div class="container">
<div class="pagetitle" style="margin: 54px 100px;">
<h1>Sub Categories</h1>
      <nav>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item">Sub Categories</li>
            <li class="breadcrumb-item">Add sub category</li>
            </ol>
      </nav>
     <a href="{{url('admin/subcategory')}}"> <button class="btn btn-primary" style="float: right;">back</button></a>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row" style="margin: 25px;">
        <div class="col-lg-12">

        <div class="card">
        <div class="card-body">
          <h5 class="card-title">Add Category</h5>

          <!-- General Form Elements -->
          <form action="{{url('admin/create-subcategory')}}" method="post">
            @csrf
            <div class="row mb-3">
              <label for="inputText" class="col-sm-2 col-form-label">Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="name" required>
              </div>
            </div>
            

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Select</label>
              <div class="col-sm-10">
                <select class="form-select" aria-label="Default select example" name="category_id" required>
                @foreach ($categories as $cat)
                  <option value="{{$cat->id}}">{{$cat->name}}</option>
                @endforeach
                
                </select>
              </div>
            </div>

            <div class="row mb-3">
             
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary" name="submitbtn" value="save">Save</button>
              </div>
            </div>

          </form><!-- End General Form Elements -->

        </div>
      </div>

          

        </div>

        
      </div>
    </section>
</div>
@endsection