@extends('layouts.app')
   
@section('content')
<div class="container">
<div class="pagetitle" style="margin: 54px 100px;">
<h1>Categories</h1>
      <nav>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item">Categories</li>
            
            </ol>
      </nav>
     <a href="{{url('admin/add-category')}}"> <button class="btn btn-primary" style="float: right;">Add New Category</button></a>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row" style="margin: 25px;">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Category List</h5>

              <!-- Default Table -->
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
               <?php $i = 1; ?>
                @foreach ($categories as $cat)

                <tr>
                    <th scope="row">{{$i}}</th>
                    <td>{{$cat->name}}</td>
                    <td><a href="{{url('admin/cat-delete/'.$cat->id)}}"><button class="btn btn-danger">Delete</button></a></td>
                </tr>
                <?php $i++; ?>
                @endforeach
                  
                  
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

          

        </div>

        
      </div>
    </section>
</div>
@endsection