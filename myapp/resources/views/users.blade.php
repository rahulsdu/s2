@extends('layouts.app')
   
@section('content')
<div class="container">
<div class="pagetitle" style="margin: 54px 100px;">
<h1>Users</h1>
      <nav>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item">Users</li>
            
            </ol>
      </nav>
      <!-- <button class="btn btn-primary" style="float: right;">Add New Category</button> -->
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row" style="margin: 25px;">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">User List</h5>

              <!-- Default Table -->
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Created Date</th>
                    <th scope="col">Go to dashboard</th>
                  </tr>
                </thead>
                <tbody>
               <?php $i = 1; ?>
                @foreach ($users as $user)

                <tr>
                    <th scope="row">{{$i}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->created_at}}</td>
                    <td><a href="{{url('/home')}}"><button class="btn btn-primary">Dashboard</button></a></td>
                </tr>
                <?php $i++; ?>
                @endforeach
                  
                  
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

          

        </div>

        
      </div>
    </section>
</div>
@endsection