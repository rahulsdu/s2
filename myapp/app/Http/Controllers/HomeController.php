<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    public function index(){
        return view('login');
    }

    public function login(Request $request)
    {   
       
        $input = $request->all();
       // print_r($request->all()); die;
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
       
   
        if(Auth::attempt(array('email' => $input['email'], 'password' => $input['password'])))
        {
           
            if (Auth::user()->is_admin == 1) {
                return redirect()->route('admin.home');
            }else{
                return redirect()->route('home');
            }
        }else{
            
            return redirect()->route('login')
                ->with('error','Email-Address And Password Are Wrong.');
        }
          
    }

    public function adminHome()
    {
        return view('adminhome');
    }
    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
      }
      public function user()
      {
          return view('userhome');
      }  
}
