<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use App\Models\Subcategory;


class AdminController extends Controller
{
    public function users(){
        $users = User::where('is_admin',0)->get();
        return view('users',compact('users'));  

    }
    public function category(){
        $categories = Category::get();
        return view('category',compact('categories'));  

    }
    public function addCategory(Request $request){
        if($request->submitbtn){
            $category = new Category;
            $category->name = $request->name;
            $category->save();
            if($category->id){
                return redirect()->route('admin.category');
            }
        }else{
            return view('addcategory');
        }
          
    }
    public function subCategory(){
        $subcategories = Subcategory::with('category')->get();
        return view('subcategory',compact('subcategories'));  

    }
    public function addSubCategory(Request $request){
        if($request->submitbtn){
            $category = new Subcategory;
            $category->name = $request->name;
            $category->category_id = $request->category_id;
            $category->save();
            if($category->id){
                return redirect()->route('admin.subcategory');
            }
        }else{
            $categories = Category::get();
            return view('addsubcategory',compact('categories'));
        }
          
    }
    public function catDelete($id){
        $cat = Category::find($id);
        $cat->delete();
        return redirect()->route('admin.category'); 

    }
    public function subcatDelete($id){
        $cat = Subcategory::find($id);
        $cat->delete();
        return redirect()->route('admin.subcategory'); 

    }
}
